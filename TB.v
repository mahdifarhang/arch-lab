`timescale 1ns/1ns

module TB();

	reg clk = 1, rst = 0;

	ARM Arm(.clk(clk), .rst(rst));

	always #150 clk = ~clk;

	initial begin
		#100
		rst = 1;
		#300
		rst = 0;

		#1000000
		$stop;
	end

endmodule
